# :green_apple: Studio Culture Front-end Assessment

![walkthru](/assets/screen_walkthru.gif)

## :crystal_ball: Instructions

https://elegant-tereshkova-6faa1a.netlify.com/ (Author's recommendation: run your eyes through the readme before proceeding)

Alternatively you may clone the repo, open the folder in VSCode, select `Watch Sass` and open it on `Live Server` through a default browser of your choice.

## :construction: Unfinished work

**Generally ran out of time :clock:.**

* Inconsistent responsiveness, **piecemeal** approach to tailoring each section's appearance for each breakpoint is not ideal.

* Attempted responsive hamburger menu for main horizontal nav. Ran out of time

* Attempted the diagonally-positioned apostrophes in booking-testimonial section. out of time

* Attempted the [bootstrap carousel](https://www.w3schools.com/bootstrap4/bootstrap_carousel.asp). Should have attempted this earlier but going back later in the process meant burning up way too much time.  

* Tested very briefly on [browserstack](https://www.browserstack.com/) for cross-browser compatibility

## :bomb: Bugs & Issues

### Overall

1. I see a lot of imprecise measurements, and any precision I managed at desktop level will be lost once I started on tailoring for breakpoints. I can only manage rudimentary eyeballing (hacky margin fixes, shadows, line-heights of text) but not many calculated solutions.

2. Not familiar with [bootstrap grid system](https://getbootstrap.com/docs/4.3/layout/grid/) leading to ham-fitsed use. This resulted in responsive issues across the layout. Also there is an over-reliance on flexbox (familiar).

3. *Time management* - deciding how much time to burn to persist with accurate measurements (*very* time consuming), researching for best practices and solutions to problems, and getting *a* working solution.

4. Managing anxiety about getting a (okay) product out.

### Nav

* Inconsistent margins between links in minor nav.
* Could have used the bootstrap navbar component instead of roll-my-own

![Nav progress](/assets/nav-progress.png)

### Header

- Does not have carousel functionality
- made choice to lose the arrow when not responsive

![Header progress](/assets/header-progress.png)

### Blurb

- Not the best implementation of image-overlap. Relied on z-index and setting `height` and not `min-height` of the <section> to achieve image overlap
- Loses the image overlap at responsive screensize so kept it hidden after desktop 

![Blurb progress](/assets/blurb-progress.png)

### What We Do

- gutters are not accurate
- not convinced the bootstrap grid system was used properly

![What we do progress](/assets/whatwedo-progress.png)

### Clients Logo showcase

* Haven't figured out a way to horizontally space out the logos in a visually-consistence manner along the baseline.
* attempted at responsiveness. original plan is to degrade to a two column arrangement.
* bootstrap margins blew out the origin height measurements.
![client progress](/assets/client-progress.png)

### Services section (ticks)

* Gutters between the cards are much too wide. It should only be 18px (1.8rem). Attempted to customise the gutter in `_base.scss` but was ultimately unsuccessful.

![Services progress](/assets/services-progress.png)

### Bookings - Testimonials & Form


| Testimonials     | Form    |
| --------|-------- |
| did not have time to do the quotations  | Looked into [this CSS tricks article](https://css-tricks.com/form-validation-part-1-constraint-validation-html/) on adding regex to validate any email-type input   |
| should use padding inside a specific margin size to allow for easier responsiveness | . |

![Bookings progress](/assets/booking-progress.png)

### Footer

* probably the most stable responsive section on the whole page :(

![Footer progress](/assets/footer-progress.png)

## :cold_sweat: What could be done differently

* Reconsider the responsive approach - build out the basic structure for each section and position all the major parts first before finessing the smaller margins, measurements and alignments. Desktop-first approach is fine as long as the html structuring of divs is done well and considers all the positioning issues across differnet screensizes.

* Try for something else other than desktop-first

* Burnt a lot of time learning and researching unfortunately, eventually losing out time on the navigation bar.

* Instead of specifying `min-height` for each section, should use padding measurements so that you can achieve consistent proportions across breakpoints

* Can use a combination of css-grid and flexbox to achieve consistency and better code organisation.

## :gift: Resources used

- Bootstrap CSS framework
- Fontawesome
- Google Fonts Open Sans (3 weights)
- [Vertical Align](http://zerosixthree.se/vertical-align-anything-with-just-3-lines-of-css/) when flexbox is being unfaithful to you.
- [This article](https://vecta.io/blog/best-way-to-embed-svg) supported that using <img> is a preferable way of adding svg to the html as long as interactivity and font access is not necessary. `<embed>` and `<object>` are not ideal for simply display.